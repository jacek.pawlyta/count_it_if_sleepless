clear;
clc;

% Open the file
myFileName = 'Co60_1350V_x20_p7_sub.dat';
myFile = dir(myFileName);
myFileId=fopen(myFileName);

% Define size of the RAM for buffer
blockSize = 5000000;

% Define number of samples visualized at ones on a screen
screenSize = 2000;
position = 0;

for k = 1:fix(myFile.bytes/blockSize)+1
    data = fread(myFileId,blockSize,'uint8');
    for n = 1:(fix(blockSize/screenSize)+1)*500
        for m = 1:500
            % this is to make oscilloscope-like dataflow animation
            dataStart = ((n-1)*screenSize+m*fix(screenSize/500));
            dataStop = (n*screenSize+m*fix(screenSize/500));
            position = ['position in the file = ',num2str(k*dataStart)];
            % plot data time frame in the window frame
            plot(data(dataStart:dataStop),'b.');
            axis([0 screenSize 40 90]);
            % give me the info were in the file we are
            text(screenSize-0.9*screenSize, 80, position,'FontSize',18);
            % make graph painting dependend on the computing and visialisatio environment
            % the value below depends on the CPU processing power and display refresh rate
            pause(0.0166666667);
        end
    end
end

fclose(myFileId);
