clear;
clc;

myFileName = 'Co60_1350V_x20_p7.dat';
myFile = dir(myFileName);
myFileId=fopen(myFileName);

blockSize = 10000000;
screenSize = 2000;

for k = 1:fix(myFile.bytes/blockSize)+1
    data = fread(myFileId,blockSize,'uint8');
    for n = 1:(fix(blockSize/screenSize)+1)*250
        for m = 1:250
            dataStart = ((n-1)*screenSize+m*fix(screenSize/250));
            dataStop = (n*screenSize+m*fix(screenSize/250));
            position = ['position in the file = ',num2str(k*dataStart)];
            plot(data(dataStart:dataStop));
            axis([0 screenSize 100 150]);
            text(screenSize-0.2*screenSize, 140, position,'FontSize',18);
            pause(0.0082);
        end;
    end;
end;

fclose(myFileId);
